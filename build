#!/bin/sh

set -e

alpine=3.21

docker run \
  --rm \
  -i \
  -v 'gocache:/tmp/.go' \
  -u 0:0 \
  "alpine:$alpine" \
  chown "$(id -u):$(id -g)" /tmp/.go

# BSD/MacOS shell script is not reliable, so that we use POSIX shell script

exec docker run \
  --rm \
  -i \
  -v "$(realpath "$(dirname "$0")"):/src" \
  -v gocache:/tmp/.go \
  -w /src \
  -u "$(id -u):$(id -g)" \
  -e HOME=/tmp \
  -e GOPATH=/tmp/.go \
  -e CGO_ENABLED=0 \
  "golang:1.23.4-alpine$alpine" \
  sh -es -- "$@" <<'EOF'
go mod tidy

if [ ! -e "$(go env GOPATH)/bin/goimports" ]; then
  go install golang.org/x/tools/cmd/goimports@latest
fi

find $(go list -f . ./...) \
  -name '*.go' \
  -exec "$(go env GOPATH)/bin/goimports" -l -w {} +

# gofmt -s -w ./...
go vet ./...

if [ "$#" -gt 0 -a "$1" = all ]; then
  all=1
else
  all=0
fi

set -- go \
  build \
  -a \
  -x \
  -ldflags '-s -w -extldflags -static' \
  -v

if [ $all -eq 0 ]; then
  "$@" -o server ./main
else
  GOOS=linux GOARCH=386 "$@" -o server.386.bin ./main
  GOOS=linux GOARCH=amd64 "$@" -o server.amd64.bin ./main
  GOOS=windows GOARCH=386 "$@" -o server.386.exe ./main
  GOOS=windows GOARCH=amd64 "$@" -o server.amd64.exe ./main
fi

printf 'OK!\n'
EOF
