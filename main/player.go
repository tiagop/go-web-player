package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"
)

type Status struct {
	Count   int64   `json:"count"`
	Paused  bool    `json:"paused"`
	Time    float64 `json:"time"`
	Changed int64   `json:"changed"`
}

type Update struct {
	Paused bool    `json:"paused"`
	Time   float64 `json:"time"`
}

const longPolling = time.Duration(30) * time.Second

var (
	status = &Status{
		Count:   0,
		Paused:  true,
		Time:    0,
		Changed: time.Now().UnixNano(),
	}
	mutex    = sync.RWMutex{}
	channels = []chan int64{}
	changed  = sync.Mutex{}
	modTime  = int64(0)
)

func main() {
	log.SetOutput(os.Stdout)
	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	router := http.NewServeMux()
	router.HandleFunc("/", func(w http.ResponseWriter, req *http.Request) {
		log.Printf("Client connect method %s path %s remote %s\n", req.Method, req.URL.Path, req.RemoteAddr)
		if req.Method == http.MethodGet {
			if req.URL.Path == "/" {
				w.Header().Set("Content-type", "text/html; charset=utf-8")
				w.Write([]byte(`<!doctype html>
<html lang="pt-br">
<head>
	<title>Player</title>
</head>
<body>
<style type="text/css">
html {
	height: 100%;
}
body {
	height: 100%;
	margin: 0;
	background-color: #000;
	overflow: auto;
	text-align: center;
}
video {
	margin-width: auto;
	max-width: 100%;
	max-height: 100%;
}
</style>
<script type="text/javascript">
!function() {
	window.addEventListener('load', function() {
		var video = document.getElementsByTagName("video")[0];
		var status = {
			count: 0,
			paused: true,
			time: 0,
			changed: 0
		};
		var canUpdate = false;
		var trigger = false;
		var changing = false;
		function sameStatus(sameState) {
			var projection = status.time;
			if (!status.paused) {
				projection += (new Date().getTime() - status.changed) / 1000;
			}
			return (status.paused === video.paused || !sameState) &&
				(
					!video.paused &&
					video.currentTime >= projection - 1 &&
					video.currentTime <= projection + 1 ||
					video.currentTime === projection
				);
		}
		function changeState() {
			if (changing) {
				return;
			}
			if (video.seeking || video.readyState !== 4) {
				if (!changing) {
					changing = true;
					setTimeout(function() {
						changing = false;
						changeState()
					}, 500);
				}
				return;
			}
			if (sameStatus(true)) {
				return;
			}
			if (video.paused && !canUpdate) {
				adjustStatus(true);
				return;
			}
			var update = new XMLHttpRequest();
			update.open('POST', '/update', true);
			update.send(JSON.stringify({
				paused: video.paused,
				time: video.currentTime
			}));
			var last = status.count;
			setTimeout(function() {
				if (last === status.count) {
					adjustStatus(false);
				}
			}, 5000);
		}
		video.addEventListener('play', changeState);
		video.addEventListener('pause', changeState);
		video.addEventListener('seeked', changeState);
		function adjustStatus(force) {
			if (trigger) {
				return;
			}
			var same = sameStatus(canUpdate);
			if (!same) {
				if (status.paused !== video.paused) {
					if (status.paused) {
						video.pause();
					} else {
						var promise = video.play();
						if (!canUpdate && promise !== undefined) {
							promise.catch(function() {
								if (!status.paused) {
									video.muted = true;
									video.play();
								}
							});
						}
					}
				}
				var projection = status.time;
				if (!status.paused) {
					projection += (new Date().getTime() - status.changed) / 1000;
				}
				if (
					video.currentTime < projection - 1 ||
					video.currentTime > projection + 1 ||
					video.paused && video.currentTime !== projection
				) {
					video.currentTime = projection;
				}
			} else if (!canUpdate && !video.paused) {
				canUpdate = true;
				if (status.paused) {
					force = true;
				}
			}
			var hardSame = same && (canUpdate || status.paused === video.paused);
			if (!hardSame || force || video.seeking) {
				trigger = true;
				var reforce = !hardSame || video.seeking;
				setTimeout(function() {
					trigger = false;
					adjustStatus(reforce);
				}, 500);
			}
		}
		function readStatus() {
			var req = new XMLHttpRequest();
			req.onreadystatechange = function() {
				if (this.readyState === 4) {
					if (this.status === 200) {
						var data = JSON.parse(this.responseText);
						if (status.count > data.count) {
							video.src = '/stream?rev=' + new Date().getTime().toString(16);
						}
						status.count = data.count;
						status.paused = data.paused;
						status.time = data.time;
						status.changed = new Date().getTime() - data.changed;
						adjustStatus(true);
						readStatus();
					} else {
						setTimeout(readStatus, 5000);
					}
				}
			}
			req.open('GET', '/status', true);
			req.setRequestHeader('X-Count', status.count);
			req.send();
		}
		readStatus();
	});
}();
</script>
	<video controls>
		<source src="/stream?rev=` + strconv.FormatInt(time.Now().Unix(), 16) + `" type="video/mp4">
	</video>
</body>
</html>`))
				return
			}
			if req.URL.Path == "/stream" {
				file, err := os.Open("v.mp4")
				defer file.Close()
				if err != nil {
					w.WriteHeader(http.StatusNotFound)
					return
				}
				fi, err := file.Stat()
				if err != nil {
					w.WriteHeader(http.StatusInternalServerError)
					return
				}
				fmod := fi.ModTime().UnixNano()
				if fmod != modTime {
					modTime = fmod
					go func() {
						mutex.Lock()
						defer mutex.Unlock()
						log.Println("Reset counter due to file change!")
						status.Count = int64(0)
						status.Changed = time.Now().UnixNano()
						status.Paused = true
						status.Time = float64(0)
						go func() {
							changed.Lock()
							defer changed.Unlock()
							for _, channel := range channels {
								channel <- int64(0)
								close(channel)
							}
							channels = []chan int64{}
						}()
					}()
				}
				size := fi.Size()
				hrange := req.Header.Get("Range")
				srange := int64(0)
				erange := int64(0)
				esrange := false
				eerange := false
				rlimit := int64(0)
				erlimit := false
				status := http.StatusOK
				if hrange != "" {
					if !strings.HasPrefix(hrange, "bytes=") || strings.Contains(hrange, ",") {
						w.WriteHeader(http.StatusRequestedRangeNotSatisfiable)
						return
					}
					arr := strings.SplitN(hrange[6:], "-", 2)
					if len(arr) != 2 {
						w.WriteHeader(http.StatusRequestedRangeNotSatisfiable)
						return
					}
					if arr[0] != "" {
						n, err := strconv.ParseInt(arr[0], 10, 64)
						if err != nil {
							w.WriteHeader(http.StatusRequestedRangeNotSatisfiable)
							return
						}
						srange = n
						esrange = true
					}
					if arr[1] != "" {
						n, err := strconv.ParseInt(arr[1], 10, 64)
						if err != nil {
							w.WriteHeader(http.StatusRequestedRangeNotSatisfiable)
							return
						}
						erange = n
						eerange = true
					}
					status = http.StatusPartialContent
					if !esrange {
						if erange > size {
							srange = 0
						} else {
							srange = size - erange
						}
						erange = size - 1
					} else if !eerange || erange >= size {
						erange = size - 1
					}
					if erange >= size || srange >= size {
						w.WriteHeader(http.StatusRequestedRangeNotSatisfiable)
						return
					}
					w.Header().Set("Content-Range", fmt.Sprintf("bytes %d-%d/%d", srange, erange, size))
					if srange > 0 {
						_, err := file.Seek(srange, io.SeekStart)
						if err != nil {
							w.WriteHeader(http.StatusInternalServerError)
							return
						}
					}
					if erange < size-1 {
						rlimit = erange - srange + 1
						erlimit = true
					}
					w.Header().Set("Content-Length", strconv.FormatInt(erange-srange+1, 10))
				} else {
					w.Header().Set("Content-Length", strconv.FormatInt(size, 10))
				}
				w.Header().Set("Content-Type", "video/mp4")
				w.Header().Set("Accept-Ranges", "bytes")
				w.Header().Set("Last-Modified", fi.ModTime().Format(time.RFC1123))
				w.Header().Set("Connection", "close")
				w.WriteHeader(status)
				chunk := make([]byte, 1024)
				bread := int64(0)
				for {
					n, err := file.Read(chunk)
					if err != nil || n == 0 {
						break
					}
					if erlimit {
						if bread > rlimit {
							break
						}
						if bread+int64(n) > rlimit {
							n = int(rlimit - bread)
						}
						bread += int64(n)
					}
					n2, err := w.Write(chunk[0:n])
					if n2 != n || err != nil {
						break
					}
				}
				return
			}
			if req.URL.Path == "/status" {
				count := req.Header.Get("X-Count")
				if count != "" {
					ncount, err := strconv.ParseInt(count, 10, 64)
					if err != nil {
						ncount = int64(0)
					}
					if func() bool {
						mutex.RLock()
						defer mutex.RUnlock()
						return ncount == status.Count
					}() {
						channel := make(chan int64)
						func() {
							changed.Lock()
							defer changed.Unlock()
							channels = append(channels, channel)
						}()
						if func() bool {
							mutex.RLock()
							defer mutex.RUnlock()
							return ncount == status.Count
						}() {
							select {
							case <-channel:
							case <-time.After(longPolling):
							}
						}
						go func() {
							changed.Lock()
							defer changed.Unlock()
							for i, current := range channels {
								if current == channel {
									channels[i] = channels[len(channels)-1]
									channels = channels[:len(channels)-1]
									break
								}
							}
						}()
					}
				}
				rstatus := func() *Status {
					mutex.RLock()
					defer mutex.RUnlock()
					return &Status{
						Count:   status.Count,
						Paused:  status.Paused,
						Time:    status.Time,
						Changed: (time.Now().UnixNano() - status.Changed) / int64(time.Millisecond),
					}
				}()
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusOK)
				json.NewEncoder(w).Encode(rstatus)
				return
			}
		} else if req.Method == http.MethodPost {
			if req.URL.Path == "/update" {
				var update Update
				err := json.NewDecoder(req.Body).Decode(&update)
				if err != nil {
					w.WriteHeader(http.StatusBadRequest)
					return
				}
				go func() {
					now := time.Now().UnixNano()
					mutex.Lock()
					defer mutex.Unlock()
					projection := status.Time
					if !status.Paused {
						projection += float64((now-status.Changed)/int64(time.Millisecond)) / 1000
					}
					if status.Paused != update.Paused || update.Time < projection-1 || update.Time > projection+1 {
						status.Count++
						status.Changed = now
						status.Paused = update.Paused
						status.Time = update.Time
						log.Printf("Update %+v\n", status)
						count := status.Count
						go func() {
							changed.Lock()
							defer changed.Unlock()
							for _, channel := range channels {
								go func(channel chan int64) {
									channel <- count
									close(channel)
								}(channel)
							}
							channels = []chan int64{}
						}()
					}
				}()
				w.WriteHeader(http.StatusOK)
				return
			}
		}
		w.WriteHeader(http.StatusNotFound)
	})

	addr, ok := os.LookupEnv("SERVER_LISTEN")
	if !ok {
		addr = ":7777"
	}

	server := &http.Server{
		Addr:           addr,
		Handler:        router,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	go func() {
		err := server.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			log.SetOutput(os.Stderr)
			log.Fatalf("listen: %s\n", err)
		}
	}()
	log.Printf("serving at %s...\n", addr)

	<-done
	log.Println("server stopped.")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer func() {
		cancel()
	}()

	err := server.Shutdown(ctx)
	if err != nil {
		log.SetOutput(os.Stderr)
		log.Fatalf("shutdown failed: %+v", err)
	}
	log.Println("server exited.")
}
